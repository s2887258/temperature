
import nl.utwente.di.FarenheitToCelcius.TemperatureCalculator;
import org.junit.jupiter.api.Assertions;
import org.junit.Test;
        //∗∗∗ Tests t h e Quoter ∗/
public class TestTemperatureCalculator {
    @Test
    public void testTemp1() throws Exception {
        TemperatureCalculator temperatureCalculator = new TemperatureCalculator();
        double price = temperatureCalculator.getTemperature("1");
        Assertions.assertEquals(33.8, price, 0.0 , "Temperature in Fahrenheit 1 " );
    }
}
