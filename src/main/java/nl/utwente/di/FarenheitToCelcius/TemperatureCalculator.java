package nl.utwente.di.FarenheitToCelcius;

public class TemperatureCalculator {
    public double getTemperature(String temp) {
        System.out.println(Integer.parseInt(temp));
        return (double) (Integer.parseInt(temp) * 9) / 5 + 32;
    }
}
